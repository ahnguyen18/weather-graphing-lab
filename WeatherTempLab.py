import psycopg2
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#this is comment

connectionString = "dbname='ahnguyen18_db' user='ahnguyen18'"
connection = psycopg2.connect(connectionString)

cursor = connection.cursor()

sqlStatement1 = "SELECT EXTRACT(year FROM regdate) as Year, AVG(High_temp) as Average_max_temp, AVG(Low_temp) as Average_min_temp, SUM(PRPC) as Total_precipitation FROM weather GROUP BY EXTRACT(year FROM regdate);"

cursor.execute(sqlStatement1)


records = cursor.fetchall()

Year = []
Average_max_temp = []
Average_min_temp = []
Total_precipitation = []

for row in records[:200]:
    Year.append(row[0])
    Average_max_temp.append(float(row[1]))
    Average_min_temp.append(float(row[2]))
    Total_precipitation.append(float(row[3]))

fig = plt.figure(figsize = (17, 8))
plt.scatter(Year, Average_max_temp, color ='red')
plt.plot(Year, Average_max_temp, color ='red')
plt.scatter(Year, Average_min_temp, color ='green')
plt.plot(Year, Average_min_temp, color ='green')
plt.xlabel('Year')
plt.ylabel('Average Temperature (fahrenheit - Fo)')
plt.savefig("Average Temperature vs Year.png")

fig = plt.figure(figsize = (17, 8)) 
plt.scatter(Year, Total_precipitation, color ='blue')
plt.plot(Year, Total_precipitation, color ='blue')
plt.xlabel('Year')
plt.ylabel('Total Precipitation (mm)')
plt.savefig("Total Precipitation vs Year.png")
