SELECT EXTRACT(year FROM regdate) as Year, AVG(High_temp) as Average_max_temp, AVG(Low_temp) as Average_min_temp, SUM(PRPC) as Total_precipitation 
FROM weather
GROUP BY EXTRACT(year FROM regdate);
                 
                 
