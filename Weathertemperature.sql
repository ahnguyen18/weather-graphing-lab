drop table if exists weather cascade;
CREATE table weather(
regdate date,
PRPC decimal(9,6),
High_temp integer,
Low_temp integer
);

\COPY weather(regdate, PRPC, High_temp, Low_temp) FROM 'rww-weather-data.csv' DELIMITER ',' CSV HEADER;